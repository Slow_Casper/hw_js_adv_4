const url = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");
const btnFilms = document.querySelector("#btnFilms");
const btnCharacters = document.querySelector("#btnCharacters");

const loader = document.querySelector(".loader-icon");

class StarWars {
    constructor(url) {
        this.url = url;
    }

    async load() {  
        return await fetch(this.url).then((response) => {
            return response.json();
        });
    }
	
	renderFilms(root, {id, name, openingCrawl}) {
		const divFilm = document.createElement("div");
		divFilm.classList.add("film-name")

		const pID = document.createElement("p");
		pID.textContent = `Планета №: ${id}`

		const pName = document.createElement("p");
		pName.textContent = `Назва планети: ${name};`

		const pOpen = document.createElement("p");
		pOpen.textContent = openingCrawl;

		divFilm.append(pID, pName, pOpen);
		root.append(divFilm)
	}

    async loadFilmCharacters(film) {
		const divFilms = document.createElement("div");
		divFilms.classList.add("film-name")
		const pFilmName = document.createElement("p");
		pFilmName.textContent = film.name;

		const charactersList = document.createElement("ol");

        for (const characterUrl of film.characters) {
            const character = await fetch(characterUrl)
                .then(response => response.json())
                .then(el => {
					const characterItem = document.createElement("li");
					characterItem.textContent = el.name;
					charactersList.append(characterItem);
                });
        }
		divFilms.append(pFilmName, charactersList);
		return divFilms

		
    }
}

const starWars = new StarWars(url);


btnFilms.addEventListener("click", (e) => {
	e.preventDefault();
	document.querySelectorAll("p").forEach((el) => {el.remove()});
	document.querySelectorAll(".film-name").forEach((el) => {el.remove()})
	loader.style.display = "block";

	starWars.load()
		.then(async films => {
			for(const film of films) {
				await starWars.renderFilms(root, film)
				loader.style.display = "none";
			}
		})
	}
)

btnCharacters.addEventListener("click", (e) => {
	e.preventDefault();
	document.querySelectorAll("p").forEach((el) => {el.remove()});
	document.querySelectorAll(".film-name").forEach((el) => {el.remove()})
	loader.style.display = "block";
	
	starWars.load()
		.then(async films => {
			for(const film of films) {
				const divFilms = await starWars.loadFilmCharacters(film);
				loader.style.display = "none";
				root.append(divFilms);
			}
		})
})


